import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NavigationStart, Router, RouterModule, Routes} from '@angular/router';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {MainComponent} from './main/main.component';
import {BreadcrumbsComponent} from './breadcrumbs/breadcrumbs.component';
import {HttpClientModule} from '@angular/common/http';
import {MagazineIssueService} from './magazine-issue.service';
import {MagazineContentComponent} from './magazine-content/magazine-content.component';
import {YearMagazinesComponent} from './year-magazines/year-magazines.component';
import {MagazineArticleComponent} from './magazine-article/magazine-article.component';
import {ImageCarouselComponent} from './image-carousel/image-carousel.component';

// определение маршрутов
const appRoutes: Routes = [
  {path: '', component: YearMagazinesComponent},
  {path: ':year', component: YearMagazinesComponent},
  {path: ':year/:number', component: MagazineContentComponent},
  {path: ':year/:number/:article', component: MagazineArticleComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    BreadcrumbsComponent,
    MagazineContentComponent,
    YearMagazinesComponent,
    MagazineArticleComponent,
    ImageCarouselComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule
  ],
  providers: [MagazineIssueService],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private magazineService: MagazineIssueService, private router: Router) {
    router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        // console.log(event.url);
        magazineService.setCurrentUrl('russian_reporter' + event.url);
      }
    });
  }
}
