import {Component, OnInit} from '@angular/core';
import {Breadcrumbs, MagazineIssueService} from '../magazine-issue.service';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.css']
})
export class BreadcrumbsComponent implements OnInit {

  breadcrumbs$: Observable<Breadcrumbs>;

  constructor(private magazineService: MagazineIssueService) {
  }

  ngOnInit() {
    this.breadcrumbs$ = this.magazineService.getBreadcrumbs$().pipe(
      tap(breadcrumbs => console.log(breadcrumbs)),
    );
  }
}
