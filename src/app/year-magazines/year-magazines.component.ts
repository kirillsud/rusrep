import {Component, OnInit} from '@angular/core';
import {ArticleYears, Magazine, MagazineIssueService} from '../magazine-issue.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-year-magazines',
  templateUrl: './year-magazines.component.html',
  styleUrls: ['./year-magazines.component.css']
})
export class YearMagazinesComponent implements OnInit {

  content$: Observable<Magazine[][]>;
  years$: Observable<ArticleYears>;

  constructor(private magazineService: MagazineIssueService) {
  }

  ngOnInit() {
    this.content$ = this.magazineService.getIssueByYear$();
    this.years$ = this.magazineService.getAllAndCurrentYears$();
  }
}
