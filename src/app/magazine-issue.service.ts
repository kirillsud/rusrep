import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';

export interface ArticleMetadata {
  name: string;
  link: string;
  year: string;
  number: string;
}

export interface ArticlesMetadata {
  title: string;
  articles: ArticleMetadata[];
}

export interface ArticleYears {
  years_list: any[];
  current_year: any;
}

export interface Magazine {
  title: string;
  date: string;
  pic: string;
  pic_src: string;
  link: string;
  year: string;
  number: string;
}

export interface Article {
  text: string;
  img: HTMLCollectionOf<HTMLImageElement>;
  header: string;
  author: string;
}

export interface Breadcrumbs {
  year: string;
  number: string;
  name: string;
}

@Injectable()
export class MagazineIssueService {

  private currentUrl = 'russian_reporter/';
  private readonly currentHtml$ = new BehaviorSubject<Document>(undefined);

  constructor(private httpClient: HttpClient) {
    this.loadHtml();
  }

  // Прокся
  // lcp --proxyUrl http://expert.ru

  // Загрузка номера журнала
  getIssue$() {
    return this.currentHtml$.pipe(
      filter(Boolean),
      map((parsedHtml: Document) => {
        const also_li_list = parsedHtml.getElementsByClassName('also_li_list'); // содержание
        const content: ArticlesMetadata[] = [];

        if (also_li_list.length !== 0) {

          for (let i = 0; i < also_li_list.length; i++) {

            const li = also_li_list[i].children;

            for (let k = 0; k < li.length; k++) {
              const title = li[k].getElementsByClassName('main_title_lm')[0].innerHTML;
              const articles = li[k].getElementsByTagName('li');
              const article: ArticleMetadata[] = [];
              for (let a = 0; a < articles.length; a++) {
                const ahref = articles[a].getElementsByTagName('a');
                article[a] = <ArticleMetadata>{
                  'name': ahref[0].innerText,
                  'link': ahref[0].getAttribute('href').split('/')[4],
                  'year': ahref[0].getAttribute('href').split('/')[2],
                  'number': ahref[0].getAttribute('href').split('/')[3]
                };
              }

              content.push(<ArticlesMetadata>{
                'title': title,
                'articles': article,
              });
            }
          }
        }
        return content;
      })
    );
  }

  getIssueCover$() {
    return this.currentHtml$.pipe(
      filter(Boolean),
      map((parsedHtml: Document) => {
        if (parsedHtml.getElementsByClassName('media_thumb').length !== 0) {
          const pic = parsedHtml.getElementsByClassName('media_thumb')[0].firstElementChild.getAttribute('src');
          // pic = pic.indexOf("jpg");
          // http://localhost:4200/data/public/548719/548822/cover-1_rusrep_05_jpg_192x256_crop_q100.jpg 404 (Not Found)
          return 'http://expert.ru' + pic.slice(0, pic.indexOf('jpg') - 1) + '.jpg';
        }
      }),
    );
  }

  // Берем хлебные крошки
  getBreadcrumbs$() {
    return this.currentHtml$.pipe(
      filter(Boolean),
      map(() => {
        const address = this.getCurrentUrl().split('/');

        const breadcrumbs: Breadcrumbs = {
          'year': address[1] || '',
          'number': address[2] || '',
          'name': address[3] || ''
        };

        return breadcrumbs;
      })
    );
  }

  // Парсим все номера за определенный год
  getIssueByYear$() {
    return this.currentHtml$.pipe(
      filter(Boolean),
      map((parsedHtml: Document) => {
        const issues: Magazine[] = [];

        const pics = parsedHtml.getElementsByClassName('issue-pict');
        const title = parsedHtml.getElementsByClassName('issue-title');
        const date = parsedHtml.getElementsByClassName('issue-date');

        for (let i = 0; i < pics.length; i++) {
          issues[i] = <Magazine>{
            'title': title[i].innerHTML,
            'date': date[i].innerHTML,
            'pic': pics[i].innerHTML,
            'pic_src': pics[i].firstElementChild.getAttribute('src'),
            'link': title[i].getAttribute('href'),
            'year': title[i].getAttribute('href').split('/')[2],
            'number': title[i].getAttribute('href').split('/')[3],
          };
        }
        const result: Magazine[][] = [];
        for (let a = 0; a < issues.length; a += 4) {
          result.push(issues.slice(a, a + 4));
        }

        return result;
      })
    );
  }

  getAllAndCurrentYears$() {
    return this.currentHtml$.pipe(
      filter(Boolean),
      map((parsedHtml: Document) => {
        const tabs = parsedHtml.getElementsByClassName('minitabs')[0].children;
        const years_list = [];
        let current_year;
        if (tabs.length !== 0) {
          for (let i = 0; i < tabs.length; i++) {
            years_list[i] = tabs[i].getElementsByClassName('mtabs-cent')[0].innerHTML;
            if (tabs[i].className.search('act-mtab') !== -1) {
              current_year = years_list[i];
            }
          }

          return <ArticleYears>{
            'years_list': years_list,
            'current_year': current_year
          };
        }
      }),
    );
  }

  getArticle$() {
    return this.currentHtml$.pipe(
      filter(Boolean),
      map((parsedHtml: Document) => {
        const text = parsedHtml.getElementsByClassName('__rusindustry_text');
        // console.log(text);
        if (text.length !== 0) {
          const img = parsedHtml.images;
          const header = parsedHtml.getElementsByClassName('__rusindustry_header')[0].innerHTML;
          const author = parsedHtml.getElementsByClassName('author')[0].innerHTML;

          return <Article>{
            'text': text[0].innerHTML.replace(/src="\/data\//g, 'src="//expert.ru/data/'),
            'img': img,
            'header': header,
            'author': author
          };
        }
      }),
    );
  }

  public get(url: string): Observable<any> {
    return this.httpClient.get(url, {responseType: 'text'});
  }

  public setCurrentUrl(value: string) {
    this.currentUrl = value;
    this.loadHtml();
  }

  public getCurrentUrl() {
    return this.currentUrl;
  }

  // Загрузка страницы по URL
  private loadHtml() {
    this.get('http://localhost:8010/proxy/' + this.currentUrl)
      .subscribe(
        value => {
          const parser = new DOMParser();
          const parsedHtml = parser.parseFromString(value, 'text/html');

          this.currentHtml$.next(parsedHtml);
        },
        error => {
          // error - объект ошибки
          console.error('Can not load html', error);
        }
      );
  }
}
