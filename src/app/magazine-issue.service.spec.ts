import { TestBed, inject } from '@angular/core/testing';

import { MagazineIssueService } from './magazine-issue.service';

describe('MagazineIssueService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MagazineIssueService]
    });
  });

  it('should be created', inject([MagazineIssueService], (service: MagazineIssueService) => {
    expect(service).toBeTruthy();
  }));
});
