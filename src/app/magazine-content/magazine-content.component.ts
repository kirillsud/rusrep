import {Component, OnInit} from '@angular/core';
import {ArticlesMetadata, MagazineIssueService} from '../magazine-issue.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-magazine-content',
  templateUrl: './magazine-content.component.html',
  styleUrls: ['./magazine-content.component.css']
})
export class MagazineContentComponent implements OnInit {

  content$: Observable<ArticlesMetadata[]>;
  cover$: Observable<string>;

  constructor(private magazineService: MagazineIssueService) {
  }

  ngOnInit() {
    this.content$ = this.magazineService.getIssue$();
    this.cover$ = this.magazineService.getIssueCover$();
  }

}
