import {Component, OnInit} from '@angular/core';
import {Article, MagazineIssueService} from '../magazine-issue.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-magazine-article',
  templateUrl: './magazine-article.component.html',
  styleUrls: ['./magazine-article.component.css']
})
export class MagazineArticleComponent implements OnInit {

  content$: Observable<Article>;

  constructor(private magazineService: MagazineIssueService) {
  }

  ngOnInit() {
    this.content$ = this.magazineService.getArticle$();
  }

}
